package calculadora;

public class Operaciones {
	
	public double operarCalculo(double n2, double n1, String simbolo) {
		double resultado = 0;
		switch (simbolo) {
		
		case "+":
			resultado = n1 + n2;							
			break;
		case "-":
			resultado = n1 - n2;
			break;
		case "X":
			resultado = n1 * n2;
			break;
		case "/":
			resultado = n1 / n2;
			break;
		default:
			break;
		}		
		
		return resultado;
	}
}
