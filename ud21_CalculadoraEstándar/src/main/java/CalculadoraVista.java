

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JLabel;
import java.awt.BorderLayout;

import javax.swing.DefaultListModel;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.SwingConstants;

import calculadora.Operaciones;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.Font;
import javax.swing.JList;

public class CalculadoraVista {

	private JFrame frame;
	private JLabel lblResultado1;
	private JLabel lblResultado2;
	private double resultado = 0;
	private String simbolo;
	private double n1;
	private double n2;
	private JLabel lblHistorial;
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					CalculadoraVista window = new CalculadoraVista();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public CalculadoraVista() {
		initialize();
	}
	public void mostarSimbolo(String letraSimbolo) {
		
		if(letraSimbolo.equals("+") || letraSimbolo.equals("-") || letraSimbolo.equals("/") || letraSimbolo.equals("X")) {
			this.simbolo = letraSimbolo;
			
		} else {
			n1 = Double.parseDouble(lblResultado1.getText());
			lblResultado1.setText(letraSimbolo);
		}
			
	}
	public void operacion() {
		Operaciones oper = new Operaciones();
		n2 = Double.valueOf(lblResultado1.getText());
		resultado = oper.operarCalculo(n1, n2, simbolo);
		lblResultado1.setText(String.valueOf(resultado));	
		
		String mensaje = n1 +" "+simbolo+" "+ n2 +" = "+ resultado;			
		lblHistorial.setText(mensaje);

	}
	
	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 615, 520);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		JButton btnPorcentaje = new JButton("%");
		btnPorcentaje.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
			}
		});
		btnPorcentaje.setBounds(10, 204, 89, 45);
		frame.getContentPane().add(btnPorcentaje);
		
		JButton btnCE = new JButton("CE");
		btnCE.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
			}
		});
		btnCE.setBounds(98, 204, 89, 45);
		frame.getContentPane().add(btnCE);
		
		JButton btnC = new JButton("C");
		btnC.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
			}
		});
		btnC.setBounds(186, 204, 89, 45);
		frame.getContentPane().add(btnC);
		
		JButton btnC_1 = new JButton("←");
		btnC_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			}
		});
		btnC_1.setBounds(274, 204, 89, 45);
		frame.getContentPane().add(btnC_1);
		
		JButton btnPorcentaje_1 = new JButton("%");
		btnPorcentaje_1.setBounds(10, 248, 89, 45);
		frame.getContentPane().add(btnPorcentaje_1);
		
		JButton btnPorcentaje_1_1 = new JButton("%");
		btnPorcentaje_1_1.setBounds(98, 248, 89, 45);
		frame.getContentPane().add(btnPorcentaje_1_1);
		
		JButton btnPorcentaje_1_2 = new JButton("%");
		btnPorcentaje_1_2.setBounds(186, 248, 89, 45);
		frame.getContentPane().add(btnPorcentaje_1_2);
		
		JButton btn7 = new JButton("7");
		btn7.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				mostarSimbolo("7");
			}
		});
		btn7.setBounds(10, 291, 89, 45);
		frame.getContentPane().add(btn7);
		
		JButton btn8 = new JButton("8");
		btn8.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				mostarSimbolo("8");
			}
		});
		btn8.setBounds(98, 291, 89, 45);
		frame.getContentPane().add(btn8);
		
		JButton btnDividir = new JButton("/");
		btnDividir.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				mostarSimbolo("/");
			}
		});
		btnDividir.setBounds(274, 248, 89, 45);
		frame.getContentPane().add(btnDividir);
		
		JButton btnX = new JButton("X");
		btnX.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				mostarSimbolo("X");
			}
		});
		btnX.setBounds(274, 291, 89, 45);
		frame.getContentPane().add(btnX);
		
		JButton btnC_1_3 = new JButton("9");
		btnC_1_3.setBounds(186, 291, 89, 45);
		frame.getContentPane().add(btnC_1_3);
		
		JButton btnMenos = new JButton("-");
		btnMenos.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				mostarSimbolo("-");
			}
		});
		btnMenos.setBounds(274, 335, 89, 45);
		frame.getContentPane().add(btnMenos);
		
		JButton btnMas = new JButton("+");
		btnMas.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				mostarSimbolo("+");
			}
		});
		btnMas.setBounds(274, 379, 89, 45);
		frame.getContentPane().add(btnMas);
		
		JButton btnIgual = new JButton("=");
		btnIgual.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				operacion();
			}
		});
		btnIgual.setBounds(274, 423, 89, 45);
		frame.getContentPane().add(btnIgual);
		
		lblResultado1 = new JLabel("0");
		lblResultado1.setFont(new Font("Tahoma", Font.BOLD, 40));
		lblResultado1.setHorizontalAlignment(SwingConstants.RIGHT);
		lblResultado1.setBounds(40, 117, 323, 34);
		frame.getContentPane().add(lblResultado1);
		
		lblResultado2 = new JLabel("");
		lblResultado2.setHorizontalAlignment(SwingConstants.RIGHT);
		lblResultado2.setBounds(40, 80, 323, 34);
		frame.getContentPane().add(lblResultado2);
		
		JLabel lblNewLabel = new JLabel("Estándar");
		lblNewLabel.setFont(new Font("Tahoma", Font.BOLD, 20));
		lblNewLabel.setBounds(10, 11, 115, 34);
		frame.getContentPane().add(lblNewLabel);
		
		lblHistorial = new JLabel("");
		lblHistorial.setFont(new Font("Tahoma", Font.BOLD, 18));
		lblHistorial.setVerticalAlignment(SwingConstants.TOP);
		lblHistorial.setBounds(400, 56, 189, 412);
		frame.getContentPane().add(lblHistorial);
		
		JLabel lblNewLabel_2 = new JLabel("Historial");
		lblNewLabel_2.setFont(new Font("Tahoma", Font.BOLD, 16));
		lblNewLabel_2.setBounds(400, 20, 77, 20);
		frame.getContentPane().add(lblNewLabel_2);
		
		JList listHistorial = new JList();
		listHistorial.setBounds(495, 172, 1, 1);		
		frame.getContentPane().add(listHistorial);
		
		JButton btn4 = new JButton("4");
		btn4.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				mostarSimbolo("4");
			}
		});
		btn4.setBounds(10, 335, 89, 45);
		frame.getContentPane().add(btn4);
		
		JButton btn5 = new JButton("5");
		btn5.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				mostarSimbolo("5");
			}
		});
		btn5.setBounds(98, 335, 89, 45);
		frame.getContentPane().add(btn5);
		
		JButton btn6 = new JButton("6");
		btn6.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				mostarSimbolo("6");
			}
		});
		btn6.setBounds(186, 335, 89, 45);
		frame.getContentPane().add(btn6);
		
		JButton btn1 = new JButton("1");
		btn1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				mostarSimbolo("1");
			}
		});
		btn1.setBounds(10, 379, 89, 45);
		frame.getContentPane().add(btn1);
		
		JButton btn2 = new JButton("2");
		btn2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				mostarSimbolo("2");
			}
		});
		btn2.setBounds(98, 379, 89, 45);
		frame.getContentPane().add(btn2);
		
		JButton btn3 = new JButton("3");
		btn3.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				mostarSimbolo("3");
			}
		});
		btn3.setBounds(186, 379, 89, 45);
		frame.getContentPane().add(btn3);
		
		JButton btnMenosMas = new JButton("6");
		btnMenosMas.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				mostarSimbolo("33");
			}
		});
		btnMenosMas.setBounds(10, 423, 89, 45);
		frame.getContentPane().add(btnMenosMas);
		
		JButton btn0 = new JButton("0");
		btn0.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				mostarSimbolo("0");
			}
		});
		btn0.setBounds(98, 423, 89, 45);
		frame.getContentPane().add(btn0);
		
		JButton btnComa = new JButton(",");
		btnComa.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				mostarSimbolo(",");
			}
		});
		btnComa.setBounds(186, 423, 89, 45);
		frame.getContentPane().add(btnComa);
	}
}
