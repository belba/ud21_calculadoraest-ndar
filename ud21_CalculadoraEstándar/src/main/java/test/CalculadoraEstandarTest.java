package test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

import org.junit.Test;

import calculadora.Operaciones;

class CalculadoraEstandarTest {

	Operaciones oper = new Operaciones();
	
	@Test
	void test() {
		fail("Not yet implemented");
	}
	
	@Test
	public void testSuma() {
		double resultado =  oper.operarCalculo(4, 8,"+");
		double esperar = 12;
		assertEquals(esperar, resultado);
	}
	
	@Test
	public void testResta() {
		double resultado =  oper.operarCalculo(8, 4,"-");
		double esperar = 4;
		assertEquals(esperar, resultado);
	}
	
	@Test
	public void testMutiplicar() {
		double resultado =  oper.operarCalculo(2, 4,"X");
		double esperar = 8;
		assertEquals(esperar, resultado);
	}

}
